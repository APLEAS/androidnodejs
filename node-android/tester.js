var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var http = require('http');
var PORT=8080;
var url = 'mongodb://localhost:27017/schedule';
var schedArray = [];

    /**
     *  functionality: Connects to mongo db database and gets all documents from 
	 the schedule collection.  All documents are pushed into schedArray.
     *  @author: Andrew Pleas
     **/
function findResults(){
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server.");
  findSchedule(db, function() {
      db.close();
  });
});

schedArray = [];
var findSchedule = function(db, callback) {
   var cursor =db.collection('schedule').find( );
 
   cursor.each(function(err, doc) {
      assert.equal(err, null);
      if (doc != null) {
		  schedArray.push(doc);

		console.dir(doc);
		  }
       else {
		  console.log("nothing");

         callback();
      }
   });
};

}
    /**
     *  functionality: When a request is sent it sends a string of json data to
	 the client.
     *  @author: Andrew Pleas
     **/
function handleRequest(request, response){
	findResults();
	console.log("request");
	var dataString = JSON.stringify(schedArray);
    response.end(dataString + request.url);

}

//Creates a server
var server = http.createServer(handleRequest);

server.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
	
});

